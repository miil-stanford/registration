# https://github.com/InsightSoftwareConsortium/SimpleITK-Notebooks/blob/master/Utilities/intro_animation.py

# Script for generating images illustrating the movement of images and change in
# similarity metric during registration.
#

import SimpleITK as sitk
import os

import matplotlib


import matplotlib.pyplot as plt
import numpy as np
from view_dicom_series import read_dicom_image, view_np_vol, view_np_vol_colored, color_fixed_moving
from simple_itk import alpha_blend


# Paste the two given images together. On the left will be image1 and on the right image2.
# image2 is also centered vertically in the combined image.
def write_combined_image(image1, image2, horizontal_space, file_name):
    combined_image = sitk.Image((image1.GetWidth() + image2.GetWidth() + horizontal_space,
                                max(image1.GetHeight(), image2.GetHeight())), 
                                image1.GetPixelID(), image1.GetNumberOfComponentsPerPixel())
    combined_image = sitk.Paste(combined_image, image1, image1.GetSize(), (0, 0), (0, 0))
    combined_image = sitk.Paste(combined_image, image2, image2.GetSize(), (0, 0), 
                                (image1.GetWidth()+horizontal_space, 
                                 round((combined_image.GetHeight()-image2.GetHeight())/2)))
    sitk.WriteImage(combined_image, file_name)


# Callback invoked when the StartEvent happens, sets up our new data.
def start_plot():
    global metric_values, multires_iterations
    
    metric_values = []
    multires_iterations = []


# Callback invoked when the EndEvent happens, do cleanup of data and figure.
def end_plot():
    global metric_values, multires_iterations
    
    del metric_values
    del multires_iterations
    # Close figure, we don't want to get a duplicate of the plot latter on.
    plt.close()


# Callback invoked when the IterationEvent happens, update our data and 
# save an image that includes a visualization of the registered images and
# the metric value plot.    
def save_plot(registration_method, fixed, moving, transform, file_name_prefix):

    #
    # Plotting the similarity metric values, resolution changes are marked with 
    # a blue star.
    #
    global metric_values, multires_iterations
    
    metric_values.append(registration_method.GetMetricValue())                                       
    # Plot the similarity metric values
    plt.plot(metric_values, 'r')
    plt.plot(multires_iterations, [metric_values[index] for index in multires_iterations], 'b*')
    plt.xlabel('Iteration Number',fontsize=12)
    plt.ylabel('Metric Value',fontsize=12)

    # Convert the plot to a SimpleITK image (works with the agg matplotlib backend, doesn't work
    # with the default - the relevant method is canvas_tostring_rgb())
    plt.gcf().canvas.draw()    
    plot_data = np.fromstring(plt.gcf().canvas.tostring_rgb(), dtype=np.uint8, sep='')
    plot_data = plot_data.reshape(plt.gcf().canvas.get_width_height()[::-1] + (3,))
    plot_image = sitk.GetImageFromArray(plot_data, isVector=True)

    
    #
    # Extract the central axial slice from the two volumes, compose it using the transformation
    # and alpha blend it.
    #
    alpha = 0.7
    
    central_index = round((fixed.GetSize())[2]/2)
    
    moving_transformed = sitk.Resample(moving, fixed, transform, 
                                       sitk.sitkLinear, 0.0, 
                                       moving_image.GetPixelIDValue())
    # Extract the central slice in xy and alpha blend them                                   
    combined = (1.0 - alpha)*fixed[:,:,central_index] + \
               alpha*moving_transformed[:,:,central_index]

    # Assume the alpha blended images are isotropic and rescale intensity
    # Values so that they are in [0,255], convert the grayscale image to
    # color (r,g,b).
    combined_slices_image = sitk.Cast(sitk.RescaleIntensity(combined), sitk.sitkUInt8)
    combined_slices_image = sitk.Compose(combined_slices_image,
                                         combined_slices_image,
                                         combined_slices_image)

    write_combined_image(combined_slices_image, plot_image, 0, 
                         file_name_prefix + format(len(metric_values), '03d') + '.png')

    
# Callback invoked when the sitkMultiResolutionIterationEvent happens, update the index into the 
# metric_values list. 
def update_multires_iterations():
    global metric_values, multires_iterations
    multires_iterations.append(len(metric_values))

if __name__ == '__main__':

    # Read the images
    # fixed_dir = "/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/registration/20140829_48/CT_FUSION"
    # moving_dir = "/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/registration/20140829_48/WATER: MRAC 1"
    fixed_dir = "/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/registration/20140710_76/CT_FUSION"
    moving_dir = "/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/registration/20140710_76/WATER: MRAC 1"


    fixed_image = read_dicom_image(fixed_dir, sitk.sitkFloat32)

    # Perform intensity windowing and map the intensity values
    # to [0,255] and cast to 8-bit unsigned int
    fixed_image_uint8 = sitk.Cast(sitk.IntensityWindowing(fixed_image, windowMinimum=-1000, windowMaximum=500, 
                                            outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)

    moving_image = read_dicom_image(moving_dir, sitk.sitkFloat32)

    # Perform intensity windowing and map the intensity values
    # to [0,255] and cast to 8-bit unsigned int
    moving_image_uint8 = sitk.Cast(sitk.IntensityWindowing(moving_image, windowMinimum=0, windowMaximum=1200, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)

    moving_image_resampled = sitk.Resample(moving_image_uint8, fixed_image_uint8, sitk.Transform(), sitk.sitkLinear, 0, moving_image.GetPixelID())

    # View
    # view before registration
    # stack = color_fixed_moving(np.transpose(sitk.GetArrayFromImage(moving_image_resampled),[2,1,0]), np.transpose(sitk.GetArrayFromImage(fixed_image_uint8), [2,1,0]))
    # view_np_vol_colored(stack, ordered_locations=None, rows=5, cols=5, start_with=0, show_every=5, title='Before Centering')


    # view before registration - with cropping

    fixed_image_cropped = fixed_image[120:340,80:360,:]
    moving_image_cropped = moving_image[0:260, 0:260,:]
    
    moving_image_cropped_uint8 = sitk.Cast(sitk.IntensityWindowing(sitk.Resample(moving_image_cropped, fixed_image_cropped, sitk.Transform(), sitk.sitkLinear, 0), windowMinimum=0, windowMaximum=1200, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
    fixed_image_cropped_uint8 = sitk.Cast(sitk.IntensityWindowing(fixed_image_cropped, windowMinimum=-1000, windowMaximum=500, 
                                            outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)

    view_np_vol(sitk.GetArrayFromImage(moving_image_cropped_uint8),rows=6, cols=6, start_with=0,show_every=5)
    view_np_vol(sitk.GetArrayFromImage(fixed_image_cropped_uint8),rows=6, cols=6, start_with=0,show_every=5)
    stack = color_fixed_moving(np.transpose(sitk.GetArrayFromImage(fixed_image_cropped_uint8),[2,1,0]), np.transpose(sitk.GetArrayFromImage(moving_image_cropped_uint8),[2,1,0]))
    view_np_vol_colored(stack, ordered_locations=None, rows=5, cols=5, start_with=0, show_every=5, title='Before Centering - With Cropping')


    # Initial alignment of the two volumes
    transform = sitk.CenteredTransformInitializer(fixed_image_cropped, 
                                                  moving_image_cropped, 
                                                  sitk.Euler3DTransform(), 
                                                  sitk.CenteredTransformInitializerFilter.GEOMETRY)

#     stack = color_fixed_moving(np.transpose(sitk.GetArrayFromImage(sitk.Resample(moving_image_cropped_uint8, fixed_image_cropped_uint8, transform, sitk.sitkLinear, 0, moving_image.GetPixelID())
# ),[2,1,0]), np.transpose(sitk.GetArrayFromImage(fixed_image_cropped_uint8), [2,1,0]))
#     view_np_vol_colored(stack, ordered_locations=None, rows=5, cols=5, start_with=0, show_every=5, title='After centering - With Cropping')
                                             

    # Multi-resolution rigid registration using Mutual Information
    registration_method = sitk.ImageRegistrationMethod()
    registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=50)
    registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
    registration_method.SetMetricSamplingPercentage(0.04)
    registration_method.SetInterpolator(sitk.sitkLinear)
    registration_method.SetOptimizerAsGradientDescent(learningRate=1.0, 
                                                      numberOfIterations=300, 
                                                      convergenceMinimumValue=1e-7, 
                                                      convergenceWindowSize=10)
    registration_method.SetOptimizerScalesFromPhysicalShift()
    registration_method.SetShrinkFactorsPerLevel(shrinkFactors = [4,2,1])
    registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[2,1,0])
    registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()
    registration_method.SetInitialTransform(transform)

    # Add all the callbacks responsible for ploting
    registration_method.AddCommand(sitk.sitkStartEvent, start_plot)
    registration_method.AddCommand(sitk.sitkEndEvent, end_plot)
    registration_method.AddCommand(sitk.sitkMultiResolutionIterationEvent, update_multires_iterations) 
    output_dir = '/Users/emilyanaya/Documents/Repos/preprocess_and_register/output/iteration_plot'
    if not os.path.exists(output_dir.strip(os.path.basename(output_dir))):
        os.makedirs(output_dir.strip(os.path.basename(output_dir)))
    # registration_method.AddCommand(sitk.sitkIterationEvent, lambda: save_plot(registration_method, fixed_image_cropped, moving_image_cropped, transform, output_dir))


    final_transform = registration_method.Execute(fixed_image_cropped, moving_image_cropped)

    # Always check the reason optimization terminated.
    print('Final metric value: {0}'.format(registration_method.GetMetricValue()))
    print('Optimizer\'s stopping condition, {0}'.format(registration_method.GetOptimizerStopConditionDescription()))
        
    # view registration results
    transformed_moving = sitk.Resample(moving_image_cropped_uint8, fixed_image_cropped_uint8, final_transform, sitk.sitkLinear, 0, moving_image.GetPixelID())

    stack = color_fixed_moving(np.transpose(sitk.GetArrayFromImage(transformed_moving),[2,1,0]), np.transpose(sitk.GetArrayFromImage(fixed_image_cropped_uint8), [2,1,0]))
    view_np_vol_colored(stack, ordered_locations=None, rows=5, cols=5, start_with=0, show_every=5, title='After registration - With Cropping')
          
    vols_blended = sitk.GetArrayFromImage(alpha_blend(fixed_image_cropped_uint8, transformed_moving, alpha = 0.5))   
    view_np_vol(vols_blended, ordered_locations=None, title='Alpha Blended Fixed and Moving Volumes')

