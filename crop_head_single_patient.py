import os
import shutil
import numpy as np
import SimpleITK as sitk
from dicom_functions import read_dicom_image
from view_dicom_series import view_np_vol, dicom_dir_to_np_vol
import pydicom
import pdb
import argparse
import logging
import sys
import tqdm


# opens UI for user to view and determine slice location to use when cropping head
# input is the path of the full body for a particular patient date
# saves cropped head to output path
def crop_head_single_patient(input_dir, output_dir, scantype_to_copy, scantype_to_crop, display_cropped = False):
    
    if len(scantype_to_copy) == 0 and len(scantype_to_crop) == 0:
        print("Nothing to crop or copy")
        exit(1)

    # initialize cut location
    cut = 0
    copied = 0

    # copy data if it's already head and neck data
    if len(scantype_to_copy) > 0:
        for i in range(len(scantype_to_copy)):
            
            src_dir = os.path.join(input_dir, scantype_to_copy[i])
            if not os.path.exists(src_dir):
                continue

            # dst is "output_dir / modality"
            dst_dir = os.path.join(output_dir, scantype_to_copy[i])

            print(f"Copying dicom head files for {src_dir}")

            # os.makedirs(dst_dir, exist_ok=True)
            shutil.copytree(src_dir, dst_dir)

            print(display_cropped)
            # load and display the cropped head if display_cropped True
            # sometimes nothing is copied because part of the patient images are missing for the particular scantype
            if display_cropped == True and len(os.listdir(dst_dir)) != 0:
                img = read_dicom_image(dst_dir, sitk.sitkFloat32)
                img = sitk.GetArrayFromImage(img)
                view_np_vol(img, rows=6, cols=6, start_with=0, show_every=2, title='after cropping') 
            print("Succesfully copied")
            copied = True

    # crop data if it's not head and neck data
    if copied == False and len(scantype_to_crop) > 0:
        for i in tqdm.tqdm(range(len(scantype_to_crop))):
            print(f"Cropping dicom head files for {scantype_to_crop[i]}")

            src_dir = os.path.join(input_dir, scantype_to_crop[i])
            if not os.path.exists(src_dir):
                continue

            # dst is "output_dir / scantype"
            dst_dir = os.path.join(output_dir, scantype_to_crop[i])
            os.makedirs(dst_dir, exist_ok=True)

            # choose cut location only if it's first scantype to crop
            # otherwise use the same cut location as for the other scantype
            if i==0:

                # make np volume from dicom src_dir of correctly ordered slices and view volume and determine crop location
                ordered_vol, sorted_locations = dicom_dir_to_np_vol(src_dir)
                view_np_vol(ordered_vol.transpose(2,0,1), sorted_locations, rows=10, cols=10, start_with=100, show_every=15, title='look for slice location to crop')
                cut = input("Enter your value: ")

            # dicom files start with Z - copy slices greater than cut location specified by user
            print("Copying dicom head files for {} ".format(src_dir))
            slices = [s for s in os.listdir(src_dir) if "Z" in s]
            for i in range(len(slices)):
                ds = pydicom.dcmread(os.path.join(src_dir, slices[i]))
                if ds.SliceLocation > float(cut):
                    os.system('cp {} {}'.format(os.path.join(src_dir,str(slices[i])),os.path.join(dst_dir,str(slices[i]))))
        
            # load and display the cropped head if display_cropped True
            # sometimes nothing is copied because part of the patient images are missing for the particular scantype
            if display_cropped == True and len(os.listdir(dst_dir)) != 0:
                img = read_dicom_image(dst_dir, sitk.sitkFloat32)
                img = sitk.GetArrayFromImage(img)
                view_np_vol(img, rows=6, cols=6, start_with=0, show_every=2, title='after cropping') 

            
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Interface to crop head region from single full body patient')
    parser.add_argument('--input_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_full_body_copy/20150108_88",
                        help='directory containing full body data')
    parser.add_argument('--output_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_head_with_arms/20150108_88",
                        help='directory to store cropped data')
    parser.add_argument('--scantype_to_copy', nargs="*", default=["CT_FUSION_HN", "HN_MAC", "HN_NAC", "HN_TOF"])
    parser.add_argument('--scantype_to_crop', nargs="*", default=["CT_FUSION","WB_NAC", "WB_MAC", "WB_TOF"])
    parser.add_argument('--display_cropped', type=bool, default=False, help='display cropped slices after performing crop')
   
    args = parser.parse_args()

    def die(error_message):
        print(f"Error: {error_message}", file=sys.stderr)
        exit(1)

    if not os.path.isdir(args.input_dir):
        die("Input path is not a directory")
    elif os.path.isdir(args.output_dir):
        die("Output path already exists")

        
    crop_head_single_patient(args.input_dir, args.output_dir, args.scantype_to_copy, args.scantype_to_crop, args.display_cropped)
