import cv2
import os
import shutil
import numpy as np
import SimpleITK as sitk
from dicom_functions import read_dicom_image
from view_dicom_series import view_np_vol, dicom_dir_to_np_vol, color_fixed_moving, view_np_vol_colored
import matplotlib.pyplot as plt
import pydicom
import pdb
import argparse
import logging
import sys

# opens UI for user to view and determine slice location to use when cropping head
# input is the path of the full body for a particular patient date
# saves png images to same directory
def create_three_channel_png(input_dir1, input_dir2, input_dir3, output_dir):

    png_files1 = [f for f in os.listdir(input_dir1) if f.endswith('.png')]
    png_files2 = [f for f in os.listdir(input_dir2) if f.endswith('.png')]
    png_files3 = [f for f in os.listdir(input_dir3) if f.endswith('.png')]

    if png_files1 != png_files2 or png_files1 != png_files3:
        logging.error("Paths do not contain the same images")
        exit(1)

    scantype1 = os.path.basename(input_dir1)
    scantype2 = os.path.basename(input_dir2)
    scantype3 = os.path.basename(input_dir3)
    combined_scantypes = f'{scantype1}_{scantype2}_{scantype3}'
    output_dir_name = os.path.join(output_dir,combined_scantypes)
    os.makedirs(output_dir_name, exist_ok=True)

    for png in png_files1:
        image1 = cv2.imread(os.path.join(input_dir1,png))
        image2 = cv2.imread(os.path.join(input_dir2,png))
        image3 = cv2.imread(os.path.join(input_dir3,png))
        # create new image with different channel data
        new_image = image1
        new_image[:,:,1] = image2[:,:,0]
        new_image[:,:,2] = image3[:,:,0]
        print(f'Saving image {os.path.join(output_dir_name, png)}')
        cv2.imwrite(os.path.join(output_dir_name, png), new_image)
        # plot an example image
        # if png == "20140630_98_slice46.png":
        #     fig,ax = plt.subplots(1,3,figsize=[12,12])
        #     print(new_image[:,:,0].shape)
        #     print(new_image[:,:,0].dtype)
        #     ax[0].imshow((new_image[:,:,0]),cmap='gray')
        #     ax[0].set_title('PET')
        #     ax[1].imshow((new_image[:,:,1]),cmap='gray')
        #     ax[1].set_title('WATER DIXON MR')      
        #     ax[2].imshow((new_image[:,:,2]),cmap='gray')
        #     ax[2].set_title('FAT DIXON MR')
        #     plt.show()
    
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Save axial slices as .png from single head and neck patient .mha images. \n MR and CT are assumed to be the same size and spacing. PET is resampled to be the same size and spacing as CT.')
    parser.add_argument('--input_dir1', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/png_lung_images/WB_NAC")  
    parser.add_argument('--input_dir2', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/png_lung_images/WATER: MRAC 30_transformed") 
    parser.add_argument('--input_dir3', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/png_lung_images/FAT: MRAC 30_transformed")  
    parser.add_argument('--output_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/png_lung_images/")
    args = parser.parse_args()

    def die(error_message):
        print(f"Error: {error_message}", file=sys.stderr)
        exit(1)

    if not os.path.isdir(args.input_dir1):
        die("Input path 1 is not a directory")
    if not os.path.isdir(args.input_dir2):
        die("Input path 2 is not a directory")
    if not os.path.isdir(args.input_dir3):
        die("Input path 3 is not a directory")
        
    create_three_channel_png(args.input_dir1, args.input_dir2, args.input_dir3, args.output_dir)
