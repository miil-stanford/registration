# https://github.com/InsightSoftwareConsortium/SimpleITK-Notebooks/blob/master/Utilities/intro_animation.py

# Script for generating images illustrating the movement of images and change in
# similarity metric during registration.
#

import argparse
import SimpleITK as sitk
import os

import matplotlib

import sys
import matplotlib.pyplot as plt
import numpy as np
from view_dicom_series import read_dicom_image, view_np_vol, view_np_vol_colored, color_fixed_moving
from simple_itk import alpha_blend, save_plot, start_plot, end_plot, update_multires_iterations

from register_head_single_patient import register_head_single_patient


# registered moving image and transform is saved to fixed_dir
# iteration plot is saved to fixed_dir/iter_dir
def register_head_multiple_patients(fixed_dir, fixed_name, moving_dir, display):

    dates_fixed = [d for d in os.listdir(fixed_dir) if os.path.isdir(os.path.join(fixed_dir,d))]
    dates_moving = [d for d in os.listdir(moving_dir) if os.path.isdir(os.path.join(moving_dir,d))]

    print("dates ", dates_fixed)
    
    # for each date, crop and save head or copy if already head data
    for date_fixed in dates_fixed:
        print("date_fixed", date_fixed)
        date_fixed_beg = date_fixed.split("_")[0]
        for date_moving in dates_moving:
            # print("date_moving", date_moving)
            date_moving_beg = date_moving.split("_")[0]
            print(date_moving_beg)
            if date_moving_beg == date_fixed_beg:
                print("true")
    

        # register_head_single_patient(fixed_date_dir, fixed_name, args.moving_dir, args.display)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Interface to register fixed image (.mha) and moving image (dicom series) from multiple patients')
    parser.add_argument('--fixed_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_head_with_armsbed/",
                        help='directory containing fixed .mha image')
    parser.add_argument('--fixed_name', type=str, default="CT_FUSION.mha",
                        help='name of fixed image')
    parser.add_argument('--moving_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/MR_full_body/",
                        help='directory containing moving dicom images')
    parser.add_argument('--display', type=bool, help="Display images before and after registration", default=True)

    args = parser.parse_args()

    def die(error_message):
        print(f"Error: {error_message}", file=sys.stderr)
        exit(1)

    if not os.path.isdir(args.fixed_dir):
        die("Fixed image path is not a directory")
    elif not os.path.exists(args.moving_dir):
        die("Moving image path is not a directory")
        
    register_head_multiple_patients(args.fixed_dir, args.fixed_name, args.moving_dir, args.display)

