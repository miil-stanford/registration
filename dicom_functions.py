import SimpleITK as sitk
import sys

def read_dicom_image(path, datatype):
    print("Reading Dicom directory:", path)
    reader = sitk.ImageSeriesReader()

    dicom_names = reader.GetGDCMSeriesFileNames(path)
    if not dicom_names:
        print("ERROR: given directory \"" + path + "\" does not contain a DICOM series.")
        sys.exit(1)
    else:
        print("Number of dicom images:", len(dicom_names))
        reader.SetFileNames(dicom_names)

        image = sitk.ReadImage(dicom_names, datatype)
        size = image.GetSize()
        print("Image size:", size[0], size[1], size[2])
        
        return image
