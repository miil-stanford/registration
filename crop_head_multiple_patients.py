import os
import numpy as np
import SimpleITK as sitk
from crop_head_single_patient import crop_head_single_patient
import argparse
import sys
import tqdm

# input is the path of the full body for multiple patient dates 
# saves cropped head for each patient to output path
def crop_head_multiple_patients(input_dir, output_dir, scantype_to_copy, scantype_to_crop, display_cropped = False):

    dates = [d for d in os.listdir(input_dir) if os.path.isdir(os.path.join(input_dir,d))]
    print("dates ", dates)
    
    # for each date, crop and save head or copy if already head data
    for date in tqdm.tqdm(dates):
        print("date", date)
        input_date_dir = os.path.join(input_dir,date)
        output_date_dir = os.path.join(output_dir,date)
        crop_head_single_patient(input_date_dir, output_date_dir, scantype_to_copy, scantype_to_crop, display_cropped)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Interface to crop head region from multiple full body patient')
    parser.add_argument('--input_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_full_body/",
                        help='directory containing full body data')
    parser.add_argument('--output_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_head_with_armsbed/",
                        help='directory to store cropped data')
    parser.add_argument('--scantype_to_copy', nargs="*", default=["CT_FUSION_HN", "HN_MAC", "HN_NAC", "HN_TOF"])
    parser.add_argument('--scantype_to_crop', nargs="*", default=["CT_FUSION","WB_NAC", "WB_MAC", "WB_TOF"])
    parser.add_argument('--display_cropped', type=bool, default=False, help='display cropped slices after performing crop')

    args = parser.parse_args()

    def die(error_message):
        print(f"Error: {error_message}", file=sys.stderr)
        exit(1)

    if not os.path.isdir(args.input_dir):
        die("Input path is not a directory")
    elif os.path.isdir(args.output_dir):
        die("Output path already exists")

        
    crop_head_multiple_patients(args.input_dir, args.output_dir, args.scantype_to_copy, args.scantype_to_crop, args.display_cropped)
