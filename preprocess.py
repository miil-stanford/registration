import argparse
import os
import sys

from saving_data import save_mr, save_pt_ct

parser = argparse.ArgumentParser(description='Organize dicom files into relevant subdirectories (i.e. /MR/PatientDate_PatientWeight/Modality/).')
parser.add_argument('--input_pet_ct_dir', type=str, required=True,
                    help='directory containing unorganized PET/CT data')
parser.add_argument('--input_mr_dir', type=str, required=True,
                    help='directory containing unorganized MR data')
parser.add_argument('--output_mr_dir', type=str, required=True,
                    help='directory to store organized MR data')
parser.add_argument('--output_pt_ct_dir', type=str, required=True,
                     help='directory to store organized PT and CT data')

args = parser.parse_args()

def die(error_message):
   print(f"Error: {error_message}", file=sys.stderr)
   exit(1)


# check if output MR directory exists and if not make it, then copy MR data into correctly named directories "ScanDate_PatientWeight"
if not os.path.exists(args.output_mr_dir):
    os.makedirs(args.output_mr_dir)
    print("Making full body MR dir: ", args.output_mr_dir)
    save_mr(args.input_mr_dir, args.output_mr_dir)
elif os.path.isdir(args.output_mr_dir):
    die("Output MR path already exists")
elif not os.path.isdir(args.output_mr_dir):
    die("Output MR path is not a directory")


# check if output PT+CT directory exists and if not make it, then copy PT+CT data into correctly named directories "ScanDate_PatientWeight"
if not os.path.exists(args.output_pt_ct_dir):
    os.makedirs(args.output_pt_ct_dir)
    print("Making full body PT+CT dir: " + args.output_pt_ct_dir)
    save_pt_ct(args.input_pet_ct_dir, args.output_pt_ct_dir)
elif os.path.isdir(args.output_pt_ct_dir):
    die("Output PT+CT path already exists")
elif not os.path.isdir(args.output_pt_ct_dir):
    die("Output PT+CT path is not a directory")




