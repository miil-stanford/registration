# https://github.com/InsightSoftwareConsortium/SimpleITK-Notebooks/blob/master/Utilities/intro_animation.py

# Script for generating images illustrating the movement of images and change in
# similarity metric during registration.
#

import argparse
import SimpleITK as sitk
import os

import matplotlib

import sys
import matplotlib.pyplot as plt
import numpy as np
from view_dicom_series import read_dicom_image, view_np_vol, view_np_vol_colored, color_fixed_moving
from simple_itk import alpha_blend, save_plot, start_plot, end_plot, update_multires_iterations


# registered moving image and transform is saved to fixed_dir
# iteration plot is saved to fixed_dir/iter_dir
def register_head_single_patient(fixed_dir, fixed_name, moving_dir, moving_name, scantypes_to_transform, display):

    # Read the images
    fixed_image = sitk.ReadImage(os.path.join(fixed_dir,fixed_name), sitk.sitkFloat32)
    moving_image = read_dicom_image(os.path.join(moving_dir,moving_name), sitk.sitkFloat32)

    # view before registration
    if display == True:
        moving_image_uint8 = sitk.Cast(sitk.IntensityWindowing(sitk.Resample(moving_image, fixed_image, sitk.Transform(), sitk.sitkLinear, 0), windowMinimum=0, windowMaximum=1200, 
                                                    outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
        fixed_image_uint8 = sitk.Cast(sitk.IntensityWindowing(fixed_image, windowMinimum=-1000, windowMaximum=500, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)

        stack = color_fixed_moving(np.transpose(sitk.GetArrayFromImage(fixed_image_uint8),[2,1,0]), np.transpose(sitk.GetArrayFromImage(moving_image_uint8),[2,1,0]))
        view_np_vol_colored(stack, ordered_locations=None, rows=5, cols=5, start_with=0, show_every=5, title='Before Registration')


    # Initial alignment of the two volumes
    transform = sitk.CenteredTransformInitializer(fixed_image, 
                                                  moving_image, 
                                                  sitk.Euler3DTransform(), 
                                                  sitk.CenteredTransformInitializerFilter.GEOMETRY)                                         

    # Multi-resolution rigid registration using Mutual Information
    registration_method = sitk.ImageRegistrationMethod()
    registration_method.SetMetricAsMattesMutualInformation(numberOfHistogramBins=80)
    registration_method.SetMetricSamplingStrategy(registration_method.RANDOM)
    registration_method.SetMetricSamplingPercentage(0.04)
    registration_method.SetInterpolator(sitk.sitkLinear)
    registration_method.SetOptimizerAsGradientDescent(learningRate=1.0, 
                                                      numberOfIterations=100, 
                                                      convergenceMinimumValue=1e-6, 
                                                      convergenceWindowSize=10)
    registration_method.SetOptimizerScalesFromPhysicalShift()
    registration_method.SetShrinkFactorsPerLevel(shrinkFactors = [4,2,1])
    registration_method.SetSmoothingSigmasPerLevel(smoothingSigmas=[2,1,0])
    registration_method.SmoothingSigmasAreSpecifiedInPhysicalUnitsOn()
    registration_method.SetInitialTransform(transform)

    # Add all the callbacks responsible for ploting
    registration_method.AddCommand(sitk.sitkStartEvent, start_plot)
    registration_method.AddCommand(sitk.sitkEndEvent, end_plot)
    registration_method.AddCommand(sitk.sitkMultiResolutionIterationEvent, update_multires_iterations) 

    # make directory to save iteration plots to
    iter_dir = os.path.join(os.path.dirname(fixed_dir),"iter_dir")
    os.makedirs(iter_dir, exist_ok=True)
    registration_method.AddCommand(sitk.sitkIterationEvent, lambda: save_plot(registration_method, fixed_image, moving_image, transform, f"{iter_dir}/iteration_plot"))

    final_transform = registration_method.Execute(fixed_image, moving_image)

    # Always check the reason optimization terminated and save result
    print(f'Final metric value: {registration_method.GetMetricValue()}')
    print(f'Optimizer\'s stopping condition: {registration_method.GetOptimizerStopConditionDescription()}')
    reg_result_path = os.path.join(fixed_dir, 'reg_result.txt')

    with open(reg_result_path, 'w') as f:
        f.write(f'Final metric value: {registration_method.GetMetricValue()}\n')
        f.write(f'Optimizer\'s stopping condition, {registration_method.GetOptimizerStopConditionDescription()}\n')

    
    # view registration results if display is True
    if display == True:
        matplotlib.use('macosx')
        transformed_moving = sitk.Resample(moving_image_uint8, fixed_image_uint8, final_transform, sitk.sitkLinear, 0, moving_image.GetPixelID())

        stack = color_fixed_moving(np.transpose(sitk.GetArrayFromImage(transformed_moving),[2,1,0]), np.transpose(sitk.GetArrayFromImage(fixed_image_uint8), [2,1,0]))
        view_np_vol_colored(stack, ordered_locations=None, rows=5, cols=5, start_with=0, show_every=5, title='After registration')
            
        vols_blended = sitk.GetArrayFromImage(alpha_blend(fixed_image_uint8, transformed_moving, alpha = 0.5))   
        view_np_vol(vols_blended, ordered_locations=None, title='Alpha Blended Fixed and Moving Volumes')

    sitk.WriteTransform(final_transform, os.path.join(fixed_dir,"transform.tfm"))
    writer = sitk.ImageFileWriter()
    writer.SetFileName(os.path.join(fixed_dir,f"{moving_name}_transformed.mha"))
    writer.Execute(transformed_moving)

    for i in range(len(scantypes_to_transform)):
        src_dir = os.path.join(moving_dir, scantypes_to_transform[i])
        if not os.path.exists(src_dir) or len(os.listdir(src_dir)) == 0:
            continue
        m = read_dicom_image(src_dir, sitk.sitkFloat32)
        transformed_moving = sitk.Resample(m, fixed_image, final_transform, sitk.sitkLinear, 0, m.GetPixelID())
        writer.SetFileName(os.path.join(fixed_dir,f"{scantypes_to_transform[i]}_transformed.mha"))
        writer.Execute(transformed_moving)
    

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Interface to register fixed image (.mha) and moving image (dicom series)')
    parser.add_argument('--fixed_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_head_with_armsbed/20141014_76/",
                        help='directory containing fixed .mha image')
    parser.add_argument('--fixed_name', type=str, default="CT_FUSION.mha",
                        help='name of fixed image')
    parser.add_argument('--moving_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/MR_full_body/20141014_76.2/",
                        help='directory containing moving dicom images')
    parser.add_argument('--moving_name', type=str, default="WATER: MRAC 1",
                        help='name of directory holding moving dicom images')
    parser.add_argument('--scantypes_to_transform', nargs="*", default=["ACMap TC=ON", "FAT: MRAC 1", "InPhase: MRAC 1", "OutPhase: MRAC 1"], help="Name of scantypes to transform and save")   
    parser.add_argument('--display', type=bool, help="Display images before and after registration", default=True)
    
    args = parser.parse_args()

    def die(error_message):
        print(f"Error: {error_message}", file=sys.stderr)
        exit(1)

    if not os.path.isdir(args.fixed_dir):
        die("Fixed image path is not a directory")
    elif not os.path.exists(args.moving_dir):
        die("Moving image path is not a directory")
        
    register_head_single_patient(args.fixed_dir, args.fixed_name, args.moving_dir, args.moving_name, args.scantypes_to_transform, args.display)

