import os
import shutil
import numpy as np
import SimpleITK as sitk
import pdb
import argparse
import logging
from create_png_single_patient_mask_ct_pet_with_mr import create_png_single_patient_mask_ct_pet_with_mr
import sys

## save images of all modalities using MR head as mask for PET and CT volumes
# input is the path containing multiple patient date directories that each contain .mha MR, PET, CT images
# saves png axial slices for each MR, PET, CT patient images as patientdate_scantype_slicenumber.png
def create_png_multiple_patients_mask_ct_pet_with_mr(input_dir, output_dir, rows, cols, num_slices):
    # check that it's not 20140717_99 patient
    if not os.path.exists(input_dir):
        logging.error("Input path not found")
        exit(1)
    
    dates = [d for d in os.listdir(input_dir) if os.path.isdir(os.path.join(input_dir,d)) and d != '20140717_99']
    
    # for each date, crop and save head
    for date in dates:
        input_date_path = os.path.join(input_dir,date)
        create_png_single_patient_mask_ct_pet_with_mr(input_date_path, output_dir, rows, cols, num_slices)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Save axial slices as .png from multiple head and neck patient .mha images. \n MR and CT are assumed to be the same size and spacing. PET is resampled to be the same size and spacing as CT.')
    parser.add_argument('--input_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_head_with_armsbed_rigid_affine",
                        help='directory containing full body data')
    parser.add_argument('--output_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/png_images_arms_removed")
    parser.add_argument('--rows', type=int, default=256, help='number of rows in final volume shape')
    parser.add_argument('--cols', type=int, default=256, help='number of cols in final volume shape')
    parser.add_argument('--num_slices', type=int, default=100, help='number of slices to get from each head volume')    
    args = parser.parse_args()

    def die(error_message):
        print(f"Error: {error_message}", file=sys.stderr)
        exit(1)

    if not os.path.isdir(args.input_dir):
        die("Input path is not a directory")
        
    create_png_multiple_patients_mask_ct_pet_with_mr(args.input_dir, args.output_dir, args.rows, args.cols, args.num_slices)
