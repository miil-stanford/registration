import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import argparse
from dicom_functions import read_dicom_image
import SimpleITK as sitk
import logging
import os
import pydicom

# input is directory with dicom files
# returns np volume with slices in correct order, z locations of each slice in the ordered volume
def dicom_dir_to_np_vol(dirname):
    if not os.path.exists(dirname):
        logging.error("Input path not found")
        exit(1)
    elif (os.listdir(dirname) == []):
        logging.error("Input path is empty")
        exit(1)

    img = read_dicom_image(dirname, sitk.sitkFloat32)
    slices = os.listdir(dirname)
    locations = []
    for i in range(len(slices)):
        ds = pydicom.dcmread(os.path.join(dirname, slices[i]))
        # initialize size of volume if it's the first slice
        if i == 0:
            locations = [float(ds.SliceLocation)]
            vol = np.zeros(img.GetSize())
            vol[:,:,i] = ds.pixel_array
        else:
            locations.append(float(ds.SliceLocation))
            vol[:,:,i] = ds.pixel_array

    sort_index = np.argsort(locations)
    sorted_locations = sorted(locations)

    # initialize volume to hold slices in order
    ordered_vol = np.zeros(vol.shape)
    for i in range(len(locations)):
        ordered_vol[:,:,i] = vol[:,:,sort_index[i]]
    
    return ordered_vol, sorted_locations


# view slices that make up a 3d volume "stack" of ordered dicom slices with subplots used to show slices of the stack
# locations is a list of the z locations for each slice in the stack
# sort_index is the list of the indices to correctly order locations
# stack is volume of dimensions slice x rows x cols
def view_np_vol(stack, ordered_locations=None, rows=6, cols=6, start_with=10, show_every=5, pause=None, title=''):
    fig,ax = plt.subplots(rows,cols,figsize=[12,12])
    for i in range(rows*cols):
        ind = start_with + i*show_every
        if ind<len(stack):
            if ordered_locations is not None:
                ax[int(i/rows),int(i % rows)].set_title('z %d' % ordered_locations[ind])
                ax[int(i/rows),int(i % rows)].imshow(stack[ind],cmap='gray')
            else:
                ax[int(i/rows),int(i % rows)].set_title('slice %d' % ind)
                ax[int(i/rows),int(i % rows)].imshow(stack[ind],cmap='gray')
        else:
            array = np.zeros(stack[0].shape)
            ax[int(i/rows),int(i % rows)].set_title('z %s' % "out of vol")
            ax[int(i/rows),int(i % rows)].imshow(array,cmap='gray')    
    fig.suptitle(title)
    if pause:
        plt.show(block=False)
        plt.pause(pause)
        plt.close()
    else:
        plt.show()

def view_np_slice_with_rectangle(slice, rect, title=''):
    fig,ax = plt.subplots(1,1,figsize=[12,12])
    ax.imshow(slice,cmap='gray')
    # Add the patch to the Axes
    ax.add_patch(rect)
    fig.suptitle(title)
    plt.show()

def view_np_vol_colored(stack, ordered_locations=None, rows=6, cols=6, start_with=10, show_every=5, title=''):
    fig,ax = plt.subplots(rows,cols,figsize=[12,12])
    for i in range(rows*cols):
        ind = start_with + i*show_every
        if ind<len(stack):
            if ordered_locations is not None:
                ax[int(i/rows),int(i % rows)].set_title('z %d' % ordered_locations[ind])
                ax[int(i/rows),int(i % rows)].imshow(stack[ind])
            else:
                ax[int(i/rows),int(i % rows)].set_title('slice %d' % ind)
                ax[int(i/rows),int(i % rows)].imshow(stack[ind])
        else:
            array = np.zeros(stack[0].shape)
            ax[int(i/rows),int(i % rows)].set_title('z %s' % "out of vol")
            ax[int(i/rows),int(i % rows)].imshow(array)    
    fig.suptitle(title)
    plt.show()


# moving image shown in magenta
# fixed image shown in green
# types are uint8
def color_fixed_moving(moving, fixed):

    if fixed.shape != moving.shape:
        logging.error("Fixed and moving must be the same size")
        exit(1)

    z = np.zeros((fixed.shape[0],fixed.shape[1]), np.uint8)

    blended = np.zeros((fixed.shape[2], fixed.shape[0], fixed.shape[1], 3), np.uint8)

    for slice in range(blended.shape[0]):
        blended[slice,:,:,0] = np.array(moving[:,:,slice])
        blended[slice,:,:,1] =  np.array(fixed[:,:,slice])
        blended[slice,:,:,2] = np.array(moving[:,:,slice])

    return blended



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='View volume slices from dicom slices from specific directory')
    parser.add_argument('--input_dir', type=str, help='directory containin dicom images to view')
    parser.add_argument('--start_with', type=int, default=0, help='slice in volume to start displaying')
    parser.add_argument('--rows', type=int, default=6, help='number of rows in subplots')
    parser.add_argument('--cols', type=int, default=6, help='number of cols in subplots')
    parser.add_argument('--show_every', type=int, default=5, help='skip some number of images in volume when displaying')
    parser.add_argument('--show_z_locations', type=bool, default=True, help='display z location as title of each subplot')

    args = parser.parse_args()
    stack = read_dicom_image(args.input_dir, sitk.sitkFloat32)
    if args.show_z_locations == True:
        ordered_vol, sorted_locations = dicom_dir_to_np_vol(args.input_dir)
        view_np_vol(ordered_vol.transpose(2,0,1), sorted_locations, rows=args.rows, cols=args.cols, start_with=args.start_with, show_every=args.show_every)
