
import SimpleITK as sitk
import argparse

# If the environment variable SIMPLE_ITK_MEMORY_CONSTRAINED_ENVIRONMENT is set, this will override the ReadImage
# function so that it also resamples the image to a smaller size (testing environment is memory constrained).
# %run setup_for_testing

import os
import numpy as np
from dicom_functions import read_dicom_image

import logging
import matplotlib.pyplot as plt
from PIL import Image

from view_dicom_series import view_np_vol, color_fixed_moving, view_np_vol_colored
from simple_itk import alpha_blend, mask_image_multiply



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='View fixed and moving volumes alpha blended with or without color')
    parser.add_argument('--fixed_dir', type=str, default = "/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/registration/20141015_93.0/CT_FUSION_HN", help='directory containing fixed volume dicom images')
    parser.add_argument('--moving_dir', type=str, default = "/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/registration/20141015_93.0/WATER: MRAC 1", help='directory containing moving volume dicom images')
    parser.add_argument('--start_with', type=int, default=0, help='slice in volume to start displaying')
    parser.add_argument('--rows', type=int, default=6, help='number of rows in subplots')
    parser.add_argument('--cols', type=int, default=6, help='number of cols in subplots')
    parser.add_argument('--show_every', type=int, default=5, help='skip some number of images in volume when displaying')
    parser.add_argument('--show_z_locations', action='store_true', help='display z location as title of each subplot')
    parser.add_argument('--color', action='store_true')

    args = parser.parse_args()
    

    # load the mr and ct volumes

    fixed_image = read_dicom_image(args.fixed_dir, sitk.sitkFloat32)
    
    # Perform intensity windowing and map the intensity values
    # to [0,255] and cast to 8-bit unsigned int
    fixed_image = sitk.Cast(sitk.IntensityWindowing(fixed_image, windowMinimum=-1000, windowMaximum=500, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)

    moving_image = read_dicom_image(args.moving_dir, sitk.sitkFloat32)

    # Perform intensity windowing and map the intensity values
    # to [0,255] and cast to 8-bit unsigned int
    moving_image = sitk.Cast(sitk.IntensityWindowing(moving_image, windowMinimum=0, windowMaximum=1200, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)

    moving_image_resampled = sitk.Resample(moving_image, fixed_image, sitk.Transform(), sitk.sitkLinear, 0, moving_image.GetPixelID())

    if args.color:
        stack = color_fixed_moving(np.transpose(sitk.GetArrayFromImage(fixed_image),[2,1,0]), np.transpose(sitk.GetArrayFromImage(moving_image_resampled),[2,1,0]))
        view_np_vol_colored(stack, ordered_locations=None, rows=args.rows, cols=args.cols, start_with=args.start_with, show_every=args.show_every, title='')

    else:
        #Combine the two volumes
        vols_blended = sitk.GetArrayFromImage(alpha_blend(fixed_image, moving_image_resampled, alpha = 0.5))   
        view_np_vol(vols_blended, ordered_locations=None, rows=args.rows, cols=args.cols, start_with=args.start_with, show_every=args.show_every, title='Alpha Blended Fixed and Moving Volumes')
