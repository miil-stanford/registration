import cv2
import os
import shutil
import numpy as np
import SimpleITK as sitk
from dicom_functions import read_dicom_image
from view_dicom_series import view_np_vol, dicom_dir_to_np_vol, color_fixed_moving, view_np_vol_colored
from pydicom import dcmread
import pdb
import argparse
import logging
import sys

# save images of all modalities using MR head as mask for PET and CT volumes
def create_png_single_patient_mask_ct_pet_with_mr(input_dir, output_dir, rows, cols, num_slices):

    mha_names = [f for f in os.listdir(input_dir) if f.endswith('.mha')]
    # get ct image first in order to resample pet images to ct image size and spacing
    for scantype in mha_names:
        if "CT" in scantype:
            src_dir = os.path.join(input_dir, scantype)
            ct = sitk.ReadImage(src_dir)

    # get mask if image is water MR
    for scantype in mha_names:
        if "WATER" in scantype:
            src_dir = os.path.join(input_dir, scantype)
            mr = sitk.ReadImage(src_dir)
            mask = mr > 70
            vectorRadius=(9,9,9)
            kernel=sitk.sitkBall
            mask = sitk.BinaryOpeningByReconstruction(mask, vectorRadius)
            mask = sitk.BinaryMorphologicalClosing(mask, vectorRadius, kernel)
            for i in range(5):
                mask[:,:,i] = sitk.BinaryFillhole(mask[:,:,i])
            for i in range(mask.GetSize()[2]-10, mask.GetSize()[2]):
                mask[:,:,i] = sitk.BinaryFillhole(mask[:,:,i])
            mask = sitk.BinaryDilate(mask, (1,1,1), kernel)
            # view_np_vol(sitk.GetArrayFromImage(mask),start_with=0,show_every=3,pause=2)
    
    for scantype in mha_names:
        src_dir = os.path.join(input_dir, scantype)
        vol = sitk.ReadImage(src_dir)
    
        if "Phase" in scantype:
            min = 100
            max = 2000
            vol_uint8 = sitk.Cast(sitk.IntensityWindowing(vol, windowMinimum=min, windowMaximum=max, 
                                                    outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
        elif "WATER" in scantype or "FAT" in scantype:
            min = 100
            max = np.max(vol)
            vol_uint8 = sitk.Cast(sitk.IntensityWindowing(vol, windowMinimum=min, windowMaximum=max, 
                                                    outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
        elif "CT" in scantype:
            min = -700
            max = 1000
            vol_uint8 = sitk.Cast(sitk.IntensityWindowing(vol, windowMinimum=min, windowMaximum=max, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
        elif "DWI" in scantype:
            min = 100
            max = np.max(vol)
            vol_uint8 = sitk.Cast(sitk.IntensityWindowing(vol, windowMinimum=min, windowMaximum=max, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
        elif "ACMap" in scantype:
            min = 0
            max = 2000
            vol_uint8 = sitk.Cast(sitk.IntensityWindowing(vol, windowMinimum=min, windowMaximum=max, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
        elif "MAC" in scantype:
            # resample to be the same size as ct
            vol = sitk.Resample(vol, ct, sitk.Transform(), sitk.sitkLinear, 0.0)
            min = 100
            max = np.max(vol)
            vol_uint8 = sitk.Cast(sitk.IntensityWindowing(vol, windowMinimum=min, windowMaximum=max, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
        elif "TOF" in scantype:
            # resample to be the same size as ct
            vol = sitk.Resample(vol, ct, sitk.Transform(), sitk.sitkLinear, 0.0)
            min = 100
            max = np.max(vol)
            # rescale images
            # mac_dicom_imgs = os.path.listdir(os.path.join(input_dir,'WB_TOF'))
            # mac_dicom_img = mac_dicom_imgs[0]
            # ds = dcmread(mac_dicom_img)
            vol_uint8 = sitk.Cast(sitk.IntensityWindowing(vol, windowMinimum=min, windowMaximum=max, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
        elif "NAC" in scantype:
            # resample to be the same size as ct
            vol = sitk.Resample(vol, ct, sitk.Transform(), sitk.sitkLinear, 0.0)
            min = 100
            max = np.max(vol)
            vol_uint8 = sitk.Cast(sitk.IntensityWindowing(vol, windowMinimum=min, windowMaximum=max, 
                                                outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
        
        # view_np_vol(sitk.GetArrayFromImage(vol_uint8),title=f'{scantype}')
        # make images square by padding with zeros in order to resize to 256x256 without stretching image
        vol_array = sitk.GetArrayFromImage(vol_uint8)

        # remove arms
        if "CT" in scantype or "WB" in scantype:
            vol_array[sitk.GetArrayFromImage(sitk.Cast(mask,sitk.sitkFloat32)) == 0] = np.min(vol_array)
            # view_np_vol(sitk.GetArrayFromImage(vol),start_with=0,show_every=1,title=f'{scantype}')
            # view_np_vol(vol_array,start_with=0,show_every=1,title=f'{scantype}_after')

        size = np.shape(vol_array)
        if size[1] > size[2]:
            diff = abs(size[1]-size[2])
            if size[1] > size[2]:
                z = np.zeros((int(size[0]),int(size[1]),int(diff//2)))
                z = z.astype(np.uint8)
                vol_array = np.concatenate([np.concatenate([z, vol_array], -1), z], -1)
            elif size[2] > size[1]:
                z = np.zeros((int(size[0]),int(diff//2),int(size[2])))
                z = z.astype(np.uint8)
                vol_array = np.concatenate([np.concatenate([z, vol_array], -1), z], -1)
        # view_np_vol(vol_array, title=f'{scantype}')

        # resize slices to 256x256 prior to cropping
        vol_final = sitk.GetImageFromArray(vol_array)
        spacing = vol.GetSpacing()
        size = vol_final.GetSize()
        vol_final.SetSpacing(spacing)

        reference_size = (cols,rows,num_slices)
        reference_spacing = (size[0]*spacing[0]/reference_size[0], size[1]*spacing[1]/reference_size[1], size[2]*spacing[2]/reference_size[2])
        reference_image = sitk.Image(reference_size, vol_final.GetPixelIDValue())
        reference_image.SetOrigin(vol_final.GetOrigin())
        reference_image.SetSpacing(reference_spacing)
        reference_image.SetDirection(vol_final.GetDirection())

        # resample image to reference_image size and spacing
        vol_final = sitk.Resample(vol_final, reference_image, sitk.Transform(), sitk.sitkLinear, 0.0)
        vol_final_array = sitk.GetArrayFromImage(vol_final)

        # save 2D images to output dir
        dst_name1 = os.path.basename(input_dir)
        dst_name2 = scantype.strip('.mha')
        # save in 'scantype' directory - patientnumber_slicenumber.png
        dst_dir_start = os.path.join(output_dir,dst_name2)
        os.makedirs(dst_dir_start, exist_ok=True)
        # dst_dir_start = os.path.join(dst_dir_start,f'{dst_name1}_{dst_name2}').replace(': ','_').replace(' ','_')
        for i in range(num_slices):
            slice = vol_final_array[i]
            dst_dir = os.path.join(dst_dir_start,f'{dst_name1}_slice{i}.png')
            print(f"Saving {dst_dir}")
            cv2.imwrite(dst_dir, slice)

if __name__ == "__main__":
    # 20140929_62
    # 20140718_43
    parser = argparse.ArgumentParser(description='Save axial slices as .png from single head and neck patient .mha images. \n MR and CT are assumed to be the same size and spacing. PET is resampled to be the same size and spacing as CT.')
    parser.add_argument('--input_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_head_with_armsbed_rigid_affine/20150910_56",
                        help='directory containing full body data')
    parser.add_argument('--output_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/png_images_test")
    parser.add_argument('--rows', type=int, default=256, help='number of rows in final volume shape')
    parser.add_argument('--cols', type=int, default=256, help='number of cols in final volume shape')
    parser.add_argument('--num_slices', type=int, default=100, help='number of slices to get from each head volume')    
    args = parser.parse_args()

    def die(error_message):
        print(f"Error: {error_message}", file=sys.stderr)
        exit(1)

    if not os.path.isdir(args.input_dir):
        die("Input path is not a directory")
        
    create_png_single_patient_mask_ct_pet_with_mr(args.input_dir, args.output_dir, args.rows, args.cols, args.num_slices)
