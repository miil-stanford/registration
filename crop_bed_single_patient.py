import SimpleITK as sitk
import os

import matplotlib
import matplotlib.patches as patches

import skimage
from skimage.transform import hough_line, hough_line_peaks
from skimage.feature import canny
from skimage.draw import line
from skimage import data
import argparse
import sys


import matplotlib.pyplot as plt
import numpy as np
from view_dicom_series import read_dicom_image, view_np_vol, view_np_vol_colored, color_fixed_moving, view_np_slice_with_rectangle
from simple_itk import alpha_blend

# https://github.com/InsightSoftwareConsortium/SimpleITK-Notebooks/blob/master/Python/70_Data_Augmentation.ipynb
def resample_image_256(image):
    size = image.GetSize()
    # The spatial definition of the images we want to use in a deep learning framework.
    new_size = [256, 256, size[2]]
    reference_image = sitk.Image(new_size, image.GetPixelIDValue())
    reference_image.SetOrigin(image.GetOrigin())
    reference_image.SetDirection(image.GetDirection())
    reference_image.SetSpacing(
        [
            sz * spc / nsz
            for nsz, sz, spc in zip(new_size, image.GetSize(), image.GetSpacing())
        ]
    )

    return sitk.Resample(image, reference_image)

def get_user_input(inp):
    num = 1
    inp = list(inp)
    for i in range(len(inp)):
        if inp[i].isdigit():
            num = int(inp[i])
            inp.pop(i)
            break
    inp = "".join(inp)
    return inp, num

# input_dir is a directory containing head/neck dicom files
# scantype_to_crop is other modalities to crop the same way
def crop_bed_single_patient(input_dir, scantype_to_crop, display):

    for i in range(len(scantype_to_crop)):
        src_dir = os.path.join(input_dir, scantype_to_crop[i])
        if not os.path.exists(src_dir) or len(os.listdir(src_dir)) == 0:
            continue
        image = read_dicom_image(os.path.join(input_dir, scantype_to_crop[i]), sitk.sitkFloat32)
        size = image.GetSize()
        
        # choose box location only if it's first scantype to crop
        # otherwise use the same cut location as for the other scantype
        if i==0:

            dim_crop = image.GetSize()

            # Create a Rectangle patch
            volArr = sitk.GetArrayFromImage(image)
            slice = volArr[volArr.shape[0]//2, :, :]
            print(slice.shape)
            x = slice.shape[1]//4
            y = slice.shape[0]//7
            height = slice.shape[0]*1/2.15
            width = slice.shape[1]*1/2.15
            rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')

            view_np_slice_with_rectangle(slice,rect,title='Check which direction to move bounding box')

            print("Requesting input from user")
            print("If input is good type 'y', if move up 'u', down 'd', left 'l', right 'r', bigger 'b', smaller 's', end '', number (2-9) can be added to make changes faster")
            inp = input()
            ratio = size[0]/16
            inp, num = get_user_input(inp)
            while(inp != 'y'):
                if inp == 'r':
                    x = x+num*slice.shape[1]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'l':
                    x = x-num*slice.shape[1]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'u':
                    y = y-num*slice.shape[0]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'd':
                    y = y+num*slice.shape[0]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                if inp == 'ru' or inp == 'ur':
                    x = x+num*slice.shape[1]//ratio
                    y = y-num*slice.shape[0]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'lu' or inp == 'ul':
                    x = x-num*slice.shape[1]//ratio
                    y = y-num*slice.shape[0]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                if inp == 'rd' or inp == 'dr':
                    x = x+num*slice.shape[1]//ratio
                    y = y+num*slice.shape[0]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'ld' or inp == 'dl':
                    x = x-num*slice.shape[1]//ratio
                    y = y+num*slice.shape[0]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'b':
                    x = x-num*slice.shape[1]//ratio
                    y = y-num*slice.shape[0]//ratio
                    width = width + 2*num*slice.shape[0]//ratio
                    height = height + 2*num*slice.shape[1]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'bw':
                     y = y-num*slice.shape[0]//ratio
                     height = height + 2*num*slice.shape[1]//ratio
                     rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'bh':
                     x = x-num*slice.shape[1]//ratio
                     width = width + 2*num*slice.shape[0]//ratio
                     rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 's':
                    x = x+num*slice.shape[1]//ratio
                    y = y+num*slice.shape[0]//ratio
                    width = width - 2*num*slice.shape[0]//ratio
                    height = height - 2*num*slice.shape[1]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'sw':
                    x = x+num*slice.shape[1]//ratio
                    width = width - 2*num*slice.shape[0]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == 'sh':
                    y = y+num*slice.shape[0]//ratio
                    height = height - 2*num*slice.shape[1]//ratio
                    rect = patches.Rectangle((x, y), width, height, linewidth=1, edgecolor='r', facecolor='none')
                elif inp == '':
                    return
                    
                view_np_slice_with_rectangle(slice,rect)
                inp = input()    
                inp, num = get_user_input(inp)


        if i == 0:
            x_curr = x
            y_curr = y
            height_curr = height
            width_curr = width
        else:
            dim_curr = image.GetSize()
            if dim_curr != dim_crop:
                f0 = dim_curr[0]/dim_crop[0]
                f1 = dim_curr[1]/dim_crop[1]
                x_curr = int(x*f0)
                y_curr = int(y*f1)
                height_curr = int(height*f1)
                width_curr = int(width*f0)
        image_cropped = image[int(x_curr):int(x_curr+width_curr),int(y_curr):int(y_curr+height_curr),:]
        # image_cropped = resample_image_256(image_cropped)
        if display == True:
            # image_cropped_uint8 = sitk.Cast(sitk.IntensityWindowing(image_cropped, windowMinimum=-800, windowMaximum=500, 
            #                                         outputMinimum=0.0, outputMaximum=255.0), sitk.sitkUInt8)
            image_cropped_array = sitk.GetArrayFromImage(image_cropped)

            view_np_vol(image_cropped_array, title = f"Cropped volume slices: {scantype_to_crop[i]}")

        outputImageFileName = os.path.join(input_dir,f"{scantype_to_crop[i]}.mha")
        print(outputImageFileName)

        writer = sitk.ImageFileWriter()
        writer.SetFileName(outputImageFileName)
        writer.Execute(image_cropped)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Interface to crop dicom head specified by bounding box that user manipulates')
    parser.add_argument('--input_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_head_with_armsbed/20141204_106/",
                        help='directory containing dicom images to crop in x and y')
    parser.add_argument('--scantype_to_crop', nargs="*", default=["CT_FUSION","WB_NAC", "WB_MAC", "WB_TOF"])
    parser.add_argument('--display_cropped', type=bool, default=True)

    args = parser.parse_args()

    def die(error_message):
        print(f"Error: {error_message}", file=sys.stderr)
        exit(1)

    if not os.path.isdir(args.input_dir):
        die("Input path is not a directory")

        
    crop_bed_single_patient(args.input_dir, args.scantype_to_crop, args.display_cropped)

