import logging
import os
import shutil

import tqdm
from pydicom.filereader import dcmread


# copy MR data into correct grouped directories in output mr path
def save_mr(input_mr_dir, output_mr_dir):
    # remove evil files if they exists
    mr_dates = [d for d in os.listdir(input_mr_dir) if os.path.isdir(os.path.join(input_mr_dir,d))]

    for mr_date in tqdm.tqdm(mr_dates):

        mr_date_input_dir = os.path.join(input_mr_dir,mr_date)
        logging.info(f"Copying full body MR data {mr_date_input_dir} into output path {output_mr_dir}")
        
        for root, subdirs, files in os.walk(mr_date_input_dir):
            for filename in files:
                # dicom files begin with "Z"
                if "Z" not in filename:
                    continue
                mr_dicom_src_path = os.path.join(root, filename)

                # read dicom header
                ds = dcmread(mr_dicom_src_path)
                description = ds.SeriesDescription
                weight = ds.PatientWeight
                date = ds.StudyDate
                modality = ds.Modality

                if description == "" or weight == "" or date == "" or modality == "":
                    logging.warn(f"Skipping {root} because a dicom header field is empty")
                    continue

                # destination
                if "MR" in modality:
                    mr_dicom_dst_dir = os.path.join(output_mr_dir, f"{date}_{weight}", description, filename)
                    os.makedirs(os.path.dirname(mr_dicom_dst_dir), exist_ok=True)
                    logging.info(f"Copying {mr_dicom_src_path} dicom file into {mr_dicom_dst_dir}")
                    shutil.copy(mr_dicom_src_path, mr_dicom_dst_dir)
                else:
                    logging.error(f"Not MR modality: {modality} from file {mr_dicom_src_path}")
                

# copy PET and CT data into correct grouped directories in output PET and CT paths
def save_pt_ct(input_pet_ct_dir, output_pt_ct_dir):

    # remove evil files if they exists
    pet_ct_dates = [d for d in os.listdir(input_pet_ct_dir) if os.path.isdir(os.path.join(input_pet_ct_dir,d))]

    for pet_ct_date in tqdm.tqdm(pet_ct_dates):

        pet_ct_date_input_path = os.path.join(input_pet_ct_dir,pet_ct_date)

        # only copy if not previously copied
        logging.info("Copying full body PET and CT data " + pet_ct_date_input_path + " into output path " + os.path.dirname(output_pt_ct_dir))
            
        for root, subdirs, files in os.walk(pet_ct_date_input_path):
            for filename in files:
                # dicom files begin with "Z"
                if "Z" not in filename:
                    continue
                pet_ct_dicom_src_dir = os.path.join(root, filename)

                # read dicom header
                ds = dcmread(pet_ct_dicom_src_dir)
                date = ds.StudyDate
                modality = ds.Modality
                description = ds.SeriesDescription
                weight = ds.PatientWeight

                if date == "" or modality == "" or description == "" or weight == "":
                    logging.warn(f"Skipping {root} because a dicom header field is empty")
                    continue
    
                # destination
                if "PT" or "CT" in modality:
                    # PT patients have weight with decimal precision so remove decimal portion so it's an integer and matches CT patients
                    # some descriptions are CT_FUSION_HN/ and others are CT FUSION HN
                    weight = str(int(weight))
                    pet_ct_dicom_dst_dir = os.path.join(output_pt_ct_dir, f"{date}_{weight}", description.replace("/","").replace(" ","_").rstrip("_P690").rstrip("_P600").replace("_3D_","_"), filename)
                    os.makedirs(os.path.dirname(pet_ct_dicom_dst_dir), exist_ok=True)
                    logging.info(f"Copying {pet_ct_dicom_src_dir} dicom file into {pet_ct_dicom_dst_dir}")
                    shutil.copy(pet_ct_dicom_src_dir, pet_ct_dicom_dst_dir)
                else:
                    logging.error(f"Not PT or CT modality: {modality} from file {pet_ct_dicom_src_dir}")
                



