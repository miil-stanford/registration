from crop_bed_single_patient import crop_bed_single_patient

import os
import numpy as np
import SimpleITK as sitk
import argparse
import sys


# input is the path of the full body for multiple patient dates 
# saves cropped head for each patient to output path
def crop_bed_multiple_patients(input_dir, scantype_to_crop, display = True):
    dates = [d for d in os.listdir(input_dir) if os.path.isdir(os.path.join(input_dir,d))]
    print("dates ", dates)
    
    # for each date, crop and save head or copy if already head data
    for date in dates:
        print("date", date)
        input_date_dir = os.path.join(input_dir,date)
        crop_bed_single_patient(input_date_dir, scantype_to_crop, display)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Interface to crop multiple dicom heads specified by bounding box that user manipulates')
    parser.add_argument('--input_dir', type=str, default="/Users/emilyanaya/Documents/PET_CT_MR_data_preprocessed/PT_CT_head_with_armsbed/",
                        help='directory containing dicom images to crop in x and y')
    parser.add_argument('--scantype_to_crop', nargs="*", default=["CT_FUSION","WB_NAC", "WB_MAC", "WB_TOF"])
    parser.add_argument('--display', type=bool, default=True, help='display cropped slices after performing crop')

    args = parser.parse_args()

    def die(error_message):
        print(f"Error: {error_message}", file=sys.stderr)
        exit(1)

    if not os.path.isdir(args.input_dir):
        die("Input path is not a directory")

    crop_bed_multiple_patients(args.input_dir, args.scantype_to_crop, args.display)

